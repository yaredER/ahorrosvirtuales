
import 'screen2.dart';
import 'package:flutter/material.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My click App',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }

}

class HomePage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return _Contador();
  }

}

class _Contador extends State<HomePage>{
  final n1 = TextEditingController();
  final n2 = TextEditingController();
  double suma =0.0; 
  double resta = 0.0;

  int contador = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Ahorros virtuales'),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(controller: n1,keyboardType: TextInputType.number,decoration: new InputDecoration(hintText: 'Nuevo Ingreso'),),
            Text('Ingreso Total', style: TextStyle(fontSize: 28),),
            Text('$suma', style: TextStyle(fontSize: 30),),
            TextField(controller: n2,keyboardType: TextInputType.number,decoration: new InputDecoration(hintText: 'Gasto'),),

          ],
        ),
      ),
      floatingActionButton: _botones(),
    );  
  }

  Widget _botones(){
    return Row(
      children: <Widget>[
        Builder(builder: (context)=> FloatingActionButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => ListaGastos()));
        })),
        SizedBox(width: 30,),
        FloatingActionButton(child: Icon(Icons.exposure_zero), onPressed: cero),
        Expanded(child: SizedBox()),
        FloatingActionButton(child: Icon(Icons.remove), onPressed: disminuir),
        SizedBox(width: 8,),
        FloatingActionButton(child: Icon(Icons.add), onPressed: aumentar)
      ],
    );
  }

  void aumentar(){
    setState(()=>{suma = double.parse(n1.text) + suma});
  }

  void disminuir(){
    setState(()=>{suma = suma - double.parse(n2.text)});
  }

  void cero(){
    setState(()=>{suma = 0});
  }
}