import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:ahorro_virtuales/model/gasto.dart';

class Control_Db{
  Database? _database;

  Future<Database> get database async {
 final dbpath = await getDatabasesPath();
    // this is the name of our database.
    const dbname = 'todo.db';
    // this joins the dbpath and dbname and creates a full path for database.
    // ex - data/data/todo.db
    final path = join(dbpath, dbname);

    // open the connection
    _database = await openDatabase(path, version: 1, onCreate: _createDB);
    // we will create the _createDB function separately

    return _database!;
  }


  Future<void> _createDB(Database db, int version) async {
    // make sure the columns we create in our table match the todo_model field.
    await db.execute('''
      CREATE TABLE todo(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        creationDate TEXT,
        isChecked INTEGER
      )
    ''');
  }


   //Crud Methods
  // function to add data into our database
  Future<void> insertGasto(Gasto gasto) async {
    // get the connection to database
    final db = await database;
    // insert the todo
    await db.insert(
      'todo', // the name of our table
      gasto.toMap(), // the function we created in our todo_model
      conflictAlgorithm:
          ConflictAlgorithm.replace, // this will replace the duplicate entry
    );
  }

    // function to delete a  todo from our database
  Future<void> deleteGasto(Gasto gasto) async {
    final db = await database;
    // delete the todo from database based on its id.
    await db.delete(
      'todo',
      where: 'id == ?', // this condition will check for id in todo list
      whereArgs: [gasto.id],
    );
  }

  // function to fetch all the todo data from our database
  Future<List<Gasto>> getGasto() async {
    final db = await database;
    // query the database and save the todo as list of maps
    List<Map<String, dynamic>> items = await db.query(
      'todo',
      orderBy: 'id DESC',
    ); // this will order the list by id in descending order.
    // so the latest todo will be displayed on top.

    // now convert the items from list of maps to list of todo

    return List.generate(
      items.length,
      (i) => Gasto(
        id: items[i]['id'],
        title: items[i]['title'],
        description: items[i]['description'] // this will convert the Integer to boolean. 1 = true, 0 = false.
      ),
    );
  }

    // function for updating a todo in todoList
  Future<void> updateGasto(int id, String title) async {
    final db = await database;

    await db.update(
      'todo', // table name
      {
        //
        'title': title, // data we have to update
      }, //
      where: 'id == ?', // which Row we have to update
      whereArgs: [id],
    );
  }

 
}
