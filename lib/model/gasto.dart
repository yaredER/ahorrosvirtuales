class Gasto {
  int? id;
  String title;
  String description;

  Gasto({
    this.id,
    required this.title,
    required this.description,
  });

  Map<String,dynamic> toMap(){
    return{
      'id': id,
      'title': title,
      'description': description,
    };
  }

  factory Gasto.fromMap(Map<String, dynamic> map){
    return Gasto(id: map['id'],title: map['title'], description: map['description']);
  }

  @override
  String toString(){
    return 'Todo(id : $id, title : $title, description: $description)';
  }


}