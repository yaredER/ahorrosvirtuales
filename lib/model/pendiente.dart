class Pendiente{
  int? id;
  String title;

  Pendiente({
    this.id,
    required this.title,
  });

  Map<String,dynamic> toMap(){
    return{
      'id': id,
      'title': title,
    };
  }

  factory Pendiente.fromMap(Map<String, dynamic> map){
    return Pendiente(id: map['id'],title: map['title']);
  }

  @override
  String toString(){
    return 'Todo(id : $id, title : $title)';
  }


}