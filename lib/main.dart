
import 'package:flutter/material.dart';
import 'package:ahorro_virtuales/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:ahorro_virtuales/screens/ventana_consejos.dart';
import 'package:ahorro_virtuales/screens/inversion.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:ahorro_virtuales/screens/consejosinversiones.dart';
import 'package:ahorro_virtuales/screens/ventana_inversiones.dart';
//import 'package:ahorro_virtuales/screens/ventana_historial_gasto.dart';
import 'package:ahorro_virtuales/screens/pruebaVentana.dart';
import 'package:ahorro_virtuales/screens/ventana_productos.dart';

double totalIngresos = 0;
double porcentaje = 0;
double calAhorro =(totalIngresos/100)*0.5;
double nvCalAhorro = 0.0;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AHORROS VIRTUALES',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: IngresosPage(),
      routes: {
        '/ingresos': (context) => IngresosPage(),
        //'/gastos': (context) => Home(),
        
        '/gastos':(context)=>VentanaHistorialGasto(),
        '/ahorro': (context) => AhorroPage(),
        '/consejos':(context) => ConsejosBBVA(),
        '/inversionVentana':(context) => VentanaInversion(),
        '/productos':(context) => VentanaProductos(),
      },
    );
  }
}

class IngresosPage extends StatefulWidget {
  @override
  _IngresosPageState createState() => _IngresosPageState();
  
}

class _IngresosPageState extends State<IngresosPage> {
  

  void agregarIngreso(double monto) {
    setState(() {
      totalIngresos += monto;
    });
  }

  void _agregarIngresoDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        double monto = 0;
        return AlertDialog(
          title: Text('Agregar ingresoooos'),
          content: TextField(
            keyboardType: TextInputType.number,
            onChanged: (value) {
              monto = double.parse(value);
            },
          ),
          actions: <Widget>[
            ElevatedButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            ElevatedButton(
              child: Text('Agregar'),
              onPressed: () {
                agregarIngreso(monto);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AHORROS VIRTUALES'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'INGRESOS',
                    style: TextStyle(fontSize: 30),
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Total de ingresos: \$${totalIngresos.toStringAsFixed(2)}',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 30),
                  ElevatedButton(
                    child: Text('Ver historial de gastos'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/gastos');
                    },
                  ),
                  SizedBox(height: 30),
                  ElevatedButton(
                    child: Text('Ver ahorro %'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/ahorro');
                    },
                  ),
                   SizedBox(height: 30),
                  ElevatedButton(
                    child: Text('Ver Productos'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/productos');
                    },
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: ElevatedButton(
              child: Text('Agregar'),
              onPressed: _agregarIngresoDialog,
            ),
          ),
        ],
      ),
    );
  }
}


class AhorroPage extends StatefulWidget {
  @override
   _AhorroPage createState() => _AhorroPage();
}

class _AhorroPage extends State<AhorroPage>{
  void calcularAhorro() {
    setState(() {
      calAhorro = (totalIngresos/100)*0.5;
    });
  }


   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CALCULAR AHORRO'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'AHORRO',
                    style: TextStyle(fontSize: 30),
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Ahorro Calculado: \$${calAhorro.toStringAsFixed(2)}',
                    style: TextStyle(fontSize: 20),
                  ),
                  ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                children: [
                  ListTile(
                    textColor: Colors.black87,
                    tileColor: Colors.lightBlueAccent,
                    title: Text("Consejos Financieros"),
                    subtitle: Text("Te mostramos algunos de los consejos que se recomiendan para poder administrar mejor tus finanzas. (recomendado por BBVA)"),
                  ),
                  TextButton(
                    onPressed: (){
                      Navigator.pushNamed(context, '/consejos');
                    }, 
                    child: Text("Entrar")
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                children: [
                  ListTile(
                    textColor: Colors.black87,
                    tileColor: Colors.lightBlueAccent,
                    title: Text("Inversiones"),
                    subtitle: Text("Te mostramos las opciones de formas de invertir o en donde puedes invertir."),
                  ),
                  TextButton(
                    onPressed: (){
                      Navigator.pushNamed(context, '/inversionVentana');
                    }, 
                    child: Text("Entrar")
                  )
                ],
              ),
            ),
        
          ),
          
          
        ],
      ),
    );

}
}