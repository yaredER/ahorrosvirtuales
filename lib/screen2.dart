
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

class ListaGastos extends StatefulWidget {
  const ListaGastos({Key? key}) : super(key: key);
  @override
  _ListaGastos createState() => _ListaGastos();
}

class _ListaGastos extends State<ListaGastos> {
  List _cursos = [];
  Future<void> readJson() async {
    final String response =
        await rootBundle.loadString('assests/gastos.json');
    final data = await json.decode(response);
    setState(() {
      _cursos = data["gastosHechos"];
    });
  }

  @override
  void initState() {
    this.readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Lista Gastos"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            _cursos.isNotEmpty
                ? Expanded(
                    child: ListView.builder(
                      itemCount: _cursos.length,
                      itemBuilder: (context, index) {
                        return Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          key: ValueKey(_cursos[index]["id"]),
                          margin: const EdgeInsets.all(10),
                          elevation: 10,
                          child: Column(
                            children: [
                              ListTile(
                                //leading: Text(_cursos[index]['nombrecurso']),
                                title: Text(_cursos[index]["nombreGasto"]),
                                subtitle: Text(
                                  'Total Gasto: ' +
                                      _cursos[index]["gasto"],
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                              
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextButton(
                                      onPressed: () {
                                        print(ValueKey(_cursos[index]["id"]));
                                      },
                                      child: Text('Quitar Gasto')),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
