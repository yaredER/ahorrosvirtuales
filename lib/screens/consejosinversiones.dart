import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class VentanaConsejosInversiones extends StatefulWidget{
  const VentanaConsejosInversiones({Key? key}) : super(key: key);
  @override
  _VentanaConsejosInversiones createState() => _VentanaConsejosInversiones();

}

class _VentanaConsejosInversiones extends State<VentanaConsejosInversiones>{
  List _dataInver = [];

  /**
   * Metodo para leer archivos JSON
   */

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/inversion/consejos.json');
    final data = await json.decode(response);
    setState(() {
      _dataInver =  data["consejos"];
    });
  }

  @override
  void initState(){
    this.readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Consejos para invertir sin perder'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            _dataInver.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _dataInver.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_dataInver[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_dataInver[index]["titulo"]),
                        subtitle: Text(_dataInver[index]["descripcion"]),
                      ),
                    ],
                  ),
                );
              },
            )
            )
            : Container(
              
            ),
          ],
        ),
      ),
    );
  }
}