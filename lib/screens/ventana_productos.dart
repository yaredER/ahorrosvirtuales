import 'package:ahorro_virtuales/model/pendiente.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';


class VentanaProductos extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Producto Supermercado',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Productos(),
      routes: {
        '/productos':(context)=> Productos(),
        '/pendientes':(context)=> PendientePage(),
      },
    );
  }
}

class Productos extends StatefulWidget{
  const Productos({Key? key}) : super(key: key);
  @override
  _ProductosState createState() => _ProductosState();
}

class _ProductosState extends State<Productos>{
  List _dataSor = [];

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/soriana/soriana.json');
    final data = await json.decode(response);
    setState(() {
      _dataSor =  data["sorianaproductos"];
    });

    print(_dataSor);
  }
  @override
  void initState(){
    this.readJson();
    super.initState();
  }


@override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('12 Productos Mas vendidos en Soriana'),
      ),
      
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
              ElevatedButton(
                  child: Text('Hacer Lista de Mandado'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/pendientes');
                  },
                ),
_dataSor.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _dataSor.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_dataSor[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_dataSor[index]["titulo"]),
                        subtitle: Text(_dataSor[index]["precio"]),
                      ),

                    ],
                  ),
                );
              },
            )
            )
            : Container(
              
            ),
          ],
        ),
      ),
    );
  }
}

class PendientePage extends StatefulWidget{
  @override
  _PendientePagerState createState() => _PendientePagerState();
}

class _PendientePagerState extends State<PendientePage>{
  final TextEditingController _titleController = TextEditingController();
  List<Pendiente> _pendiente = [];
  static late Database _database;

  //initState
  @override
  void initState(){
    super.initState();
    _initializeDbPendiente().then((value) {
      _loadPendiente();
    });
    
  }

  Future<void> _initializeDbPendiente() async{
    _database = await openDatabase(
      join(await getDatabasesPath(),'pendiente.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE pendientes(id INTEGER PRIMARY KEY, title TEXT)'
        );
      },
      version: 1,
    );
  }

  Future<void> _loadPendiente() async {
    final List<Map<String, dynamic>> maps = await _database.query('pendientes');
    setState(() {
      _pendiente = List.generate(
        maps.length,
        (index) => Pendiente.fromMap(maps[index]),
      );
    });
  }

   Future<void> _insertPendiente() async {
    final title = _titleController.text;
    final pendiente = Pendiente(
      title: title,
    );
    await _database.insert(
      'pendientes',
      pendiente.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    _titleController.clear();
    _loadPendiente();
  }

  Future<void> _updatePendiente(Pendiente pendiente) async {
    final updatedPendiente = Pendiente(
      id: pendiente.id,
      title: _titleController.text,
    );
    await _database.update(
      'pendientes',
      updatedPendiente.toMap(),
      where: 'id = ?',
      whereArgs: [pendiente.id],
    );
    _titleController.clear();
    _loadPendiente();
  }

  Future<void> _deletePendiente(Pendiente pendiente) async {
    await _database.delete(
      'pendientes',
      where: 'id = ?',
      whereArgs: [pendiente.id],
    );
    _loadPendiente();
  }

  @override
  void dispose(){
    _database.close();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista Supermercado'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TextField(
                  controller: _titleController,
                  decoration: InputDecoration(
                    labelText: 'Titulo Producto a Comprar',
                  ),
                ),
                
                ElevatedButton(
                  onPressed: _insertPendiente,
                  child: Text('Agregar Pendiente'),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _pendiente.length,
              itemBuilder: (context, index) {
                final pendiente = _pendiente[index];
                return ListTile(
                  title: Text(pendiente.title),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _titleController.text = pendiente.title;
                          showDialog(
                            context: context,
                            builder: (_) {
                              return AlertDialog(
                                title: Text('Editar Pendiente'),
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    TextField(
                                      controller: _titleController,
                                      decoration: InputDecoration(
                                        labelText: 'Titulo Pendiente de Compra',
                                      ),
                                    ),
                                  ],
                                ),
                                actions: [
                                  ElevatedButton(
                                    onPressed: () {
                                      print("contexto--> "+context.toString());
                                      Navigator.of(context,rootNavigator: true).pop();
                                      _updatePendiente(pendiente);
                                    },
                                    child: Text('Update'),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () => _deletePendiente(pendiente),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

}

