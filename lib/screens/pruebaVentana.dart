import 'package:flutter/material.dart';
import 'package:ahorro_virtuales/model/gasto.dart';
import 'package:ahorro_virtuales/presentador/control_Db.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class VentanaHistorialGasto extends StatefulWidget {
  @override
  _VentanaHistorialGasto createState() => _VentanaHistorialGasto();
}

class _VentanaHistorialGasto extends State<VentanaHistorialGasto> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  List<Gasto> _gasto = [];
  static late Database _database;

  //initState
  @override
  void initState(){
    super.initState();
    _initializeDbGasto().then((value) {
      _loadGasto();
    });
    
  }

  Future<void> _initializeDbGasto() async{
    _database = await openDatabase(
      join(await getDatabasesPath(),'gasto.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE gastos(id INTEGER PRIMARY KEY, title TEXT, description TEXT)'
        );
      },
      version: 1,
    );
  }

  Future<void> _loadGasto() async {
    final List<Map<String, dynamic>> maps = await _database.query('gastos');
    setState(() {
      _gasto = List.generate(
        maps.length,
        (index) => Gasto.fromMap(maps[index]),
      );
    });
  }

   Future<void> _insertGasto() async {
    final title = _titleController.text;
    final description = _descriptionController.text;
    final gasto = Gasto(
      title: title,
      description: description,
    );
    await _database.insert(
      'gastos',
      gasto.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    _titleController.clear();
    _descriptionController.clear();
    _loadGasto();
  }

  Future<void> _updateGasto(Gasto gasto) async {
    final updatedGasto = Gasto(
      id: gasto.id,
      title: _titleController.text,
      description: _descriptionController.text,
    );
    await _database.update(
      'gastos',
      updatedGasto.toMap(),
      where: 'id = ?',
      whereArgs: [gasto.id],
    );
    _titleController.clear();
    _descriptionController.clear();
    _loadGasto();
  }

  Future<void> _deleteGasto(Gasto gasto) async {
    await _database.delete(
      'gastos',
      where: 'id = ?',
      whereArgs: [gasto.id],
    );
    _loadGasto();
  }

  @override
  void dispose(){
    _database.close();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historial Gasto'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TextField(
                  controller: _titleController,
                  decoration: InputDecoration(
                    labelText: 'Titulo Gasto',
                  ),
                ),
                TextField(
                  controller: _descriptionController,
                  decoration: InputDecoration(
                    labelText: 'Cantidad Gasto',
                  ),
                ),
                ElevatedButton(
                  onPressed: _insertGasto,
                  child: Text('Agregar gasto'),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _gasto.length,
              itemBuilder: (context, index) {
                final gasto = _gasto[index];
                return ListTile(
                  title: Text(gasto.title),
                  subtitle: Text(gasto.description),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          _titleController.text = gasto.title;
                          _descriptionController.text = gasto.description;
                          showDialog(
                            context: context,
                            builder: (_) {
                              return AlertDialog(
                                title: Text('Editar Gasto'),
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    TextField(
                                      controller: _titleController,
                                      decoration: InputDecoration(
                                        labelText: 'Titulo Gasto',
                                      ),
                                    ),
                                    TextField(
                                      controller: _descriptionController,
                                      decoration: InputDecoration(
                                        labelText: 'Cantidad Gasto',
                                      ),
                                    ),
                                  ],
                                ),
                                actions: [
                                  ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                      _updateGasto(gasto);
                                    },
                                    child: Text('Update'),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () => _deleteGasto(gasto),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }


  
}