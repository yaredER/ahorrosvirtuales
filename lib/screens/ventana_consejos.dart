import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

class ConsejosBBVA extends StatefulWidget{
  const ConsejosBBVA({Key? key}) : super(key: key);
  @override
  _ConsejosBBVA createState() => _ConsejosBBVA();

}

class _ConsejosBBVA extends State<ConsejosBBVA>{
  List _consejos = [];

  /**
   * Metodo para leer archivos JSON
   */

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/bbva/bbva.json');
    final data = await json.decode(response);
    setState(() {
      _consejos =  data["consejosbbva"];
    });
  }

  @override
  void initState(){
    this.readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Consejos Financieros'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            _consejos.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _consejos.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_consejos[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_consejos[index]["titulo"]),
                        subtitle: Text(_consejos[index]["descripcion"]),
                      ),
                      
                    ],
                  ),
                );
              },
            )
            )
            : Container(),
          ],
        ),
      ),
    );
  }
}