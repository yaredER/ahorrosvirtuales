import 'package:flutter/material.dart';
import 'package:ahorro_virtuales/model/gasto.dart';
import 'package:ahorro_virtuales/presentador/control_Db.dart';


class VentanaHistorialGasto extends StatefulWidget {
  VentanaHistorialGasto({Key? key}) : super(key: key);

  @override
  _VentanaHistorialGasto createState() => _VentanaHistorialGasto();
}

class _VentanaHistorialGasto extends State<VentanaHistorialGasto> {
  Control_Db _databaseHelper = Control_Db();
  List<Gasto> allGastos=[];
  bool isActive = false;
  var _controllerTitle = TextEditingController();
  var _controllerDesc = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  late int clickedNoteID;

  void getGastos() async {
    var gastoFuture = _databaseHelper.getGasto();
    await gastoFuture.then((data) {
      setState(() {
        this.allGastos = data;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getGastos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TO DO LIST"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  buildForm(_controllerTitle, "Title"),
                  buildForm(_controllerDesc, "Description"),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                buildButton("SAVE", Colors.purple, saveObject),
                //buildButton("UPDATE", Colors.yellow, updateObject),
              ],
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: allGastos.length,
                    itemBuilder: (context, index) {
                      return Card(
                        child: ListTile(
                          onTap: () {
                            setState(() {
                              _controllerTitle.text = allGastos[index].title;
                              _controllerDesc.text =
                                  allGastos[index].description;
                              
                            });
                          },
                          title: Text(allGastos[index].title),
                          subtitle: Text(allGastos[index].description),
                          trailing: GestureDetector(
                            onTap: () {
                              //_deleteGasto(allGastos[index].id);
                            },
                            child: Icon(Icons.delete),
                          ),
                        ),
                      );
                    }))
          ],
        ),
      ),
    );
  }

  buildForm(TextEditingController textEditingController, String str) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        autofocus: false,
        controller: textEditingController,
        decoration:
            InputDecoration(labelText: str, border: OutlineInputBorder()),
      ),
    );
  }

Widget buildButton(String str, Color buttonColor, VoidCallback eventFunc) {
  return ElevatedButton(
    onPressed: eventFunc,
    child: Text(str),
    style: ElevatedButton.styleFrom(
      primary: buttonColor,
    ),
  );
}


  void saveObject() {
    if (_formKey.currentState!.validate()) {
      _addGasto(Gasto( 
      id: 0,// Coloca el valor correcto para el id
      title: _controllerTitle.text,
      description:_controllerDesc.text,
    ));
    }
  }

  //CRUD METHODS
  void _addGasto(Gasto gasto) async {
    await _databaseHelper.insertGasto(gasto);

    setState(() {
      getGastos();
      _controllerTitle.text = "";
      _controllerDesc.text = "";
    });
  }


  void _deleteGasto(Gasto gasto) async {
    await _databaseHelper.deleteGasto(gasto);
    setState(() {
      getGastos();
    });
  }

  void showAlertDialog() {
    AlertDialog alertDialog = AlertDialog(
      title: Text("There is no selected note!"),
      content: Text("Please enter a note to list for updating list."),
    );
    showDialog(
      context: context,
      builder: (context) => alertDialog,
    );
  }
}