import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
String url = '';

class VentanaInversiones extends StatefulWidget{
  const VentanaInversiones({Key? key}) : super(key: key);
  @override
  _VentanaInversiones createState() => _VentanaInversiones();

}

class _VentanaInversiones extends State<VentanaInversiones>{
  List _dataInver = [];

  /**
   * Metodo para leer archivos JSON
   */

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/inversion/inversion.json');
    final data = await json.decode(response);
    setState(() {
      _dataInver =  data["inversion"];
    });
  }

  @override
  void initState(){
    this.readJson();
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Tabla de Inversiones'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            _dataInver.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _dataInver.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_dataInver[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_dataInver[index]["titulo"]),
                        subtitle: Text(_dataInver[index]["descripcion"]),
                      ),
                      TextButton(
                        onPressed: () async{
                          url = _dataInver[index]["link"];
                          print(url);
                          if(await canLaunch(url)){
                            await launch(url);
                          }else {
                            throw 'Could not launch $url';
                          }
                        }, 
                        child: Text("Entrar a GBM"),
                      ),
                    ],
                  ),
                );
              },
            )
            )
            : Container(
              
            ),
          ],
        ),
      ),
    );
  }
}