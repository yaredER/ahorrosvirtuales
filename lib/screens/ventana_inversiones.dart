import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
String url = '';

class VentanaInversion extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Inversiones',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: InversionesPage(),
      routes: {
        '/inversiones':(context)=> InversionesPage(),
        '/consejosInversiones':(context)=> ConsejosPage(),
      },
    );
  }
}

class InversionesPage extends StatefulWidget{
  const InversionesPage({Key? key}) : super(key: key);
  @override
  _InversionesPageState createState() => _InversionesPageState();
}

class _InversionesPageState extends State<InversionesPage>{
  List _dataInver = [];

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/inversion/inversion.json');
    final data = await json.decode(response);
    setState(() {
      _dataInver =  data["inversion"];
    });

    print(_dataInver);
  }
  @override
  void initState(){
    this.readJson();
    super.initState();
  }


@override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Sin miedo a invertir'),
      ),
      
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
              ElevatedButton(
                  child: Text('Ocupas Ayuda para invertir?'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/consejosInversiones');
                  },
                ),
_dataInver.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _dataInver.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_dataInver[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_dataInver[index]["titulo"]),
                        subtitle: Text(_dataInver[index]["descripcion"]),
                      ),
                      TextButton(
                        onPressed: () async{
                          final Uri url = Uri.parse(_dataInver[index]["link"]);
                          //url = _dataInver[index]["link"];
                          print(url);
                          /*if(await canLaunch(url)){
                            await launch(url);
                          }else {
                            throw 'Could not launch $url';
                          }*/
                          if(!await launchUrl(url)){
                            throw Exception('Could not launch $url');
                          }
                        }, 
                        child: Text("Entrar a GBM"),
                      ),
                    ],
                  ),
                );
              },
            )
            )
            : Container(
              
            ),
          ],
        ),
      ),
    );
  }
}

class ConsejosPage extends StatefulWidget{
  const ConsejosPage({Key? key}) : super(key: key);
  @override
  _ConsejosPagerState createState() => _ConsejosPagerState();
}

class _ConsejosPagerState extends State<ConsejosPage>{
  List _consejos = [];

  Future<void> readJson() async{
    final String response = await rootBundle.loadString('assests/json/inversion/consejos.json');
    final data = await json.decode(response);
    setState(() {
      _consejos =  data["consejos"];
    });
  }
    @override
  void initState(){
    this.readJson();
    super.initState();
  }


 @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Consejos para invertir sin perder'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(25),
        child: Column(
          children: [
            _consejos.isNotEmpty 
            ? Expanded(child: ListView.builder(
              itemCount: _consejos.length,
              itemBuilder: (context, index){
                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  key: ValueKey(_consejos[index]["id"]),
                  margin: const EdgeInsets.all(10),
                  elevation: 10,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(_consejos[index]["titulo"]),
                        subtitle: Text(_consejos[index]["descripcion"]),
                      ),
                    ],
                  ),
                );
              },
            )
            )
            : Container(
              
            ),
          ],
        ),
      ),
    );
  }
}

